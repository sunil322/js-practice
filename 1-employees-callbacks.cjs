/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require('fs');
const path = require('path');

function writeToFile(fileName, data, callback) {
    fs.writeFile(fileName, JSON.stringify(data), (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

writeToFile('data.json', data, (error) => {
    if (error) {
        console.log(error);
    } else {
        console.log("Write data successfully");
    }
});

function readFromFile(fileName, callback) {
    fs.readFile(fileName, 'utf8', (error, data) => {
        if (error) {
            callback(error);
        } else {
            callback(null, JSON.parse(data));
        }
    });
}

//Problem 1
function retrieveDataById(data, idList, callback) {

    let retrieveDataForId = data.employees.filter((employee) => {
        return idList.includes(employee.id);
    });
    let filePath = path.join(__dirname, 'output', '1-retrieve-data-id.json');
    writeToFile(filePath, retrieveDataForId, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//Problem 2
function groupDataBasedOnCompanies(data, callback) {

    let groupEmployeeByCompanies = data.employees.reduce((groupByCompanies, employee) => {
        let company = employee.company;
        if (groupByCompanies[company] === undefined) {
            groupByCompanies[company] = [];
        }
        groupByCompanies[company].push(employee);

        return groupByCompanies;
    }, {});
    let filePath = path.join(__dirname, 'output', '2-group-data-based-on-companies.json');
    writeToFile(filePath, groupEmployeeByCompanies, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//Problem 3
function getEmployeesByCompanyName(data, company, callback) {
    let employessByCompany = data.employees.filter((employee) => {
        return employee.company === company;
    });
    let filePath = path.join(__dirname, 'output', '3-get-all-data-by-company.json');
    writeToFile(filePath, employessByCompany, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//Problem 4
function removeEntryById(data, id, callback) {
    let employeesData = data.employees.filter((employee) => {
        return employee.id !== id;
    });
    let filePath = path.join(__dirname, 'output', '4-remove-entry-with-id-2.json');
    writeToFile(filePath, employeesData, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//Problem 5
function sortDataBasedOnCompanyName(data, callback) {
    let sortedData = data.employees.sort((employee1, employee2) => {
        if (employee1.company > employee2.company) {
            return 1;
        } else if (employee1.company < employee2.company) {
            return -1;
        } else {
            return employee1.id - employee2.id;
        }
    });
    let filePath = path.join(__dirname, 'output', '5-sort-data-based-on-company.json');
    writeToFile(filePath, sortedData, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//Problem 6
function swapPositionOfCompanies(data, callback) {

    let id92Data = data.employees.find((employee) => {
        return employee.id === 92;
    });
    let id93Data = data.employees.find((employee) => {
        return employee.id === 93;
    });
    let dataAfterSwap = data.employees.map((employee) => {
        if (employee.id === 92) {
            return id93Data;
        } else if (employee.id === 93) {
            return id92Data;
        } else {
            return employee;
        }
    });
    let filePath = path.join(__dirname, 'output', '6-swap-position-of-companies.json');
    writeToFile(filePath, dataAfterSwap, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

//problem 7
function addBirthdayWhereIdIsEven(data, callback) {
    let employeesData = data.employees.map((employee) => {
        if (employee.id % 2 === 0) {
            employee.birthday = new Date().toLocaleDateString();
        }
        return employee;
    });
    let filePath = path.join(__dirname, 'output', '7-add-birthday-where-id-is-even.json');
    writeToFile(filePath, employeesData, (error) => {
        if (error) {
            callback(error);
        } else {
            callback();
        }
    });
}

readFromFile('data.json', (error, data) => {
    if (error) {
        console.log(error);
    } else {
        retrieveDataById(data, [2, 13, 23], (error) => {
            if (error) {
                console.log(error);
            } else {
                groupDataBasedOnCompanies(data, (error) => {
                    if (error) {
                        console.log(error);
                    } else {
                        getEmployeesByCompanyName(data, 'Powerpuff Brigade', (error) => {
                            if (error) {
                                console.log(error);
                            } else {
                                removeEntryById(data, 2, (error) => {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        sortDataBasedOnCompanyName(data, (error) => {
                                            if (error) {
                                                console.log(error);
                                            } else {
                                                swapPositionOfCompanies(data, (error) => {
                                                    if(error){
                                                        console.log(error);
                                                    }else{
                                                        addBirthdayWhereIdIsEven(data,(error)=>{
                                                            if(error){
                                                                console.log(error);
                                                            }else{
                                                               console.log("All task finished"); 
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});