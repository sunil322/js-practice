/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well) */

const path = require('path');
const fs = require('fs');

function createAndDeleteFiles() {
    let fileNamesList = [];
    let promises = Array(2).fill(0).map(() => {
        let fileName = Math.random().toString().slice(2);
        let filePath = path.join(__dirname, `${fileName}.txt`);
        return new Promise((resolve, reject) => {
            fs.writeFile(filePath, '', (error) => {
                if (error) {
                    reject(error);
                } else {
                    fileNamesList.push(filePath);
                    resolve();
                }
            });
        });
    });

    Promise.all(promises)
        .then(() => {
            console.log("Files created successfully");
        })
        .catch((error) => {
            console.log(error);
        });

    setTimeout(() => {
        let promises = fileNamesList.map((fileNames) => {
            return new Promise((resolve, reject) => {
                fs.unlink(fileNames, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
            });
        });

        Promise.all(promises)
            .then(() => {
                console.log("Files deleted successfully");
            })
            .catch((error) => {
                console.log(error);
            });

    }, 2 * 1000);
}

createAndDeleteFiles();


/* Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function createLipsumFile(fileName) {
    return new Promise((resolve, reject) => {
        const lipsumData = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.`;

        fs.writeFile(fileName, lipsumData, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(fileName);
            }
        });
    });
}

function readLipsumWriteDataToNewFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (error, data) => {
            if (error) {
                reject(error);
            } else {
                fs.writeFile('newLipsum.txt', data, (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(fileName);
                    }
                });
            }
        })
    });
}

function deleteFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve("File deleted successfully");
            }
        });
    });
}

createLipsumFile('lipsum.txt')
    .then((fileName) => {
        return readLipsumWriteDataToNewFile(fileName);
    })
    .then((fileName) => {
        return deleteFile(fileName);
    })
    .then((success) => {
        console.log(success);
    })
    .catch((error) => {
        console.log(error);
    });

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    const loggedData = {
        user,
        activity,
        timestamp: new Date().toLocaleString()
    };
    fs.writeFile('logData.log', JSON.stringify(loggedData) + '\n', { flag: 'a' }, (error) => {
        if (error) {
            console.log(error);
        } else {
            console.log("Log saved");
        }
    });
}

const user = {
    id: 3,
    name: "Sunil"
};

login(user, 3)
    .then((loginUser) => {
        logData(loginUser, 'Login Success');
        getData()
            .then((data) => {
                logData(loginUser, 'GetData Success')
            })
            .catch((error) => {
                logData(loginUser, 'GetData Failure');
            });
    })
    .catch((error) => {
        logData(user, 'Login Failure');
    });