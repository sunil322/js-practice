
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
const fs = require('fs');
const path = require('path');
const usersUrl = 'https://jsonplaceholder.typicode.com/users';
const todosUrl = 'https://jsonplaceholder.typicode.com/todos';

function writeToFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, JSON.stringify(data), (error) => {
            if (error) {
                reject(error);
            } else {
                resolve("Data written successfully");
            }
        });
    });
}

function fetchDataFromAPI(url) {
    return new Promise((resolve, reject) => {
        fetch(url)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
            })
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                reject(error);
            });
    });
}

function fetchUserAPIForSpecificUser(usersData) {
    return new Promise((resolve, reject) => {
        let promises = usersData.map((user) => {
            return fetchDataFromAPI(`${usersUrl}?id=${user.id}`);
        });
        Promise.all(promises)
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                reject(error);
            });
    });
}

function fetchTodoAPIForSpecificUser(todosData) {
    return new Promise((resolve, reject) => {
        let promises = todosData.map((todo) => {
            return fetchDataFromAPI(`${todosUrl}?userId=${todo.userId}`);
        });
        Promise.all(promises)
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                reject(error);
            });
    });
}

//Problem 1
function fetchAllUsers() {
    fetchDataFromAPI(usersUrl)
        .then((data) => {
            const filePath = path.join(__dirname, 'fetch-output', '1-fetch-all-users.json');
            return writeToFile(filePath, data);
        })
        .then((success) => {
            console.log(success);
        })
        .catch((error) => {
            console.log(error);
        });
}

//Problem 2
function fetchAllTodos() {
    fetchDataFromAPI(todosUrl)
        .then((data) => {
            const filePath = path.join(__dirname, 'fetch-output', '2-fetch-all-todos.json');
            return writeToFile(filePath, data);
        })
        .then((success) => {
            console.log(success);
        })
        .catch((error) => {
            console.log(error);
        });
}

//Problem 3
function fetchAllUsersThenTodos() {
    fetchDataFromAPI(usersUrl)
        .then((usersData) => {
            console.log(usersData);
            return fetchDataFromAPI(todosUrl);
        })
        .then((todosData) => {
            console.log(todosData);
        })
        .catch((error) => {
            console.log(error);
        });
}

//Problem 4
function fetchAllUsersAndTheirDetails() {
    fetchDataFromAPI(usersUrl)
        .then((usersData) => {
            console.log(usersData);
            return fetchUserAPIForSpecificUser(usersData);
        })
        .then((userDetails) => {
            console.log(userDetails);
        })
        .catch((error) => {
            console.log(error);
        });
}

//Problem 5
function fetchAllTodosAndAssociatedUsers() {
    fetchDataFromAPI(todosUrl)
        .then((todosData) => {
            console.log(todosData);
            return fetchTodoAPIForSpecificUser(todosData);
        })
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.log(error);
        });
}
