const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/


const getAllItemsPriceMoreThan65 = (products) => {
    return Object.entries(products[0]).reduce((productsPriceMoreThan65, currentProduct) => {
        if (currentProduct[1].price === undefined) {
            let filteredData = currentProduct[1].filter((item) => {
                return Number(Object.entries(item)[0][1].price.slice(1)) > 65;
            });
            productsPriceMoreThan65.push({ [currentProduct[0]]: filteredData });

        } else {
            if (Number(currentProduct[1].price.slice(1)) > 65) {
                productsPriceMoreThan65.push(currentProduct);
            }
        }
        return productsPriceMoreThan65;
    }, []);
};

const getAllItemsQuanityMoreThanOne = (products) => {
    return Object.entries(products[0]).reduce((productsQuantityMoreThanOne, currentProduct) => {
        if (currentProduct[1].price === undefined) {
            let filteredData = currentProduct[1].filter((item) => {
                return Object.entries(item)[0][1].quantity > 1;
            });
            productsQuantityMoreThanOne.push({ [currentProduct[0]]: filteredData });

        } else {
            if (currentProduct[1].quantity > 1) {
                productsQuantityMoreThanOne.push(currentProduct);
            }
        }
        return productsQuantityMoreThanOne;
    }, []);
};

const getAllFragileProducts = (products) => {
    return Object.entries(products[0]).reduce((fragileProducts, currentProduct) => {
        if (currentProduct[1].price === undefined) {
            let filteredData = currentProduct[1].filter((item) => {
                return Object.entries(item)[0][1].type === 'fragile';
            });
            fragileProducts.push({ [currentProduct[0]]: filteredData });

        } else {
            if (currentProduct[1].type === 'fragile') {
                fragileProducts.push(currentProduct);
            }
        }
        return fragileProducts;
    }, []);
};

const findLeastMostExpensiveItems = (products) => {

    let mostExpensive = undefined;
    let leastExpensive = undefined;

    let leastMostProduct = Object.entries(products[0]).reduce((leastMostExpensive,product) => {

        if (product[1].price === undefined) {
            product[1].filter((item) => {
                let price = Number(Object.entries(item)[0][1].price.slice(1));
                let quantity = Number(Object.entries(item)[0][1].quantity);
                if (mostExpensive === undefined) {
                    leastMostExpensive['most'] = item;
                } else {
                    if (Number(Object.values(mostExpensive)[0].price.slice(1)) / Object.values(mostExpensive)[0].quantity < price/quantity) {
                        leastMostExpensive['most'] = item;
                    }   
                }

                let minPrice = Number(Object.entries(item)[0][1].price.slice(1));
                let minQuantity = Number(Object.entries(item)[0][1].quantity);
                if (leastExpensive === undefined) {
                    leastMostExpensive['least'] = item;
                } else {
                    if (Number(Object.values(leastExpensive)[0].price.slice(1)) / Object.values(leastExpensive)[0].quantity > minPrice/minQuantity) {
                        leastMostExpensive['least'] = item;
                    }   
                }
            })
        } else {
            if (mostExpensive === undefined) {
                mostExpensive = {[product[0]] : product[1]};
                leastMostExpensive['most'] = mostExpensive;
            } else {
                if (Number(Object.values(mostExpensive)[0].price.slice(1)) / Object.values(mostExpensive)[0].quantity < Number(product[1].price.slice(1) / product[1].quantity)) {
                    mostExpensive = {[product[0]] : product[1]} ;
                    leastMostExpensive['most'] = mostExpensive;
                }
            }

            if (leastExpensive === undefined) {
                leastExpensive = {[product[0]] : product[1]};
                leastMostExpensive['least'] = leastExpensive;
            } else {
                if (Number(Object.values(leastExpensive)[0].price.slice(1)) / Object.values(leastExpensive)[0].quantity > Number(product[1].price.slice(1) / product[1].quantity)) {
                    leastExpensive = {[product[0]] : product[1]} ;
                    leastMostExpensive['least'] = leastExpensive;
                }
            }
        }
        return leastMostExpensive;
    },{});
    return leastMostProduct;
};