const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

const getAllAvailableItems = (items) =>{ 
    let availableItems = items.filter((item)=>{
        return item.available === true;
    });
    return availableItems;
};

const getItemsBasedOnVitamin = (items,vitamin) =>{
    let itemList = items.filter((item)=>{
        if(item.contains.includes(vitamin)){
            return true;
        }
    });
    return itemList;
};
//console.log(getItemsBasedOnVitamin(items,'Vitamin C'));
//console.log(getItemsBasedOnVitamin(items,'Vitamin A'));

const groupItemsBasedOnVitamins = (items) =>{
    let groupItems = items.reduce((itemGroupsVitaminWise,currentItem)=>{
        let vitaminList = currentItem.contains.split(',');
        vitaminList.map((vitamin)=>{
            let vitaminName = vitamin.trim();
            if(itemGroupsVitaminWise.hasOwnProperty(vitaminName) === false){
                itemGroupsVitaminWise[vitaminName] = [];
            }
            itemGroupsVitaminWise[vitaminName].push(currentItem.name);
        });
        return itemGroupsVitaminWise;
    },{});
    return groupItems;
};

const sortItemsBasedOnVitaminsNumber = (items) =>{
    let sortedItems = items.sort((item1,item2)=>{
        let item1VitaminNumber = item1.contains.split(',').length;
        let item2VitaminNumber = item2.contains.split(',').length;
        return item2VitaminNumber - item1VitaminNumber;
    });
    return sortedItems;
};
