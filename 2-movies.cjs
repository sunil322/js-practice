const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

const getMoviesEarningsMoreThan500 = (favouritesMovies) =>{

    let moviesMoreThan500MEarnings = Object.entries(favouritesMovies).filter((movie)=>{
        let totalEarningsInNumeric = movie[1].totalEarnings.slice(1,movie[1].totalEarnings.length-1);
        return totalEarningsInNumeric > 500;
    });
    return Object.fromEntries(moviesMoreThan500MEarnings);
};

const moviesMoreThanThreeOscar = (favouritesMovies) =>{

    let moviesEarning500 = Object.entries(getMoviesEarningsMoreThan500(favouritesMovies));

    let moviesList = moviesEarning500.filter((movie)=>{
        return movie[1].oscarNominations > 3;
    });
    return Object.fromEntries(moviesList);
};


const findAllMoviesOfActor = (favouritesMovies) =>{

    let movieList = Object.entries(favouritesMovies).filter((movie)=>{
        if(movie[1].actors.indexOf("Leonardo Dicaprio") !== -1){
            return true;
        }else{
            return false;
        }
    });
    return Object.fromEntries(movieList);
};

const sortMoviesByImdbRating = (favouritesMovies) =>{

    let sortedMoviesData = Object.entries(favouritesMovies).sort((firstMovie,secondMovie)=>{

        let firstMovieImdbRating = firstMovie[1].imdbRating;
        let secondMovieImdbRating = secondMovie[1].imdbRating;

        if(firstMovieImdbRating > secondMovieImdbRating){
            return -1;
        }else if(firstMovieImdbRating < secondMovieImdbRating){
            return 1;
        }else{

            let firstMovieEarning = firstMovie[1].totalEarnings.slice(1,firstMovie[1].totalEarnings.length-1);
            let secondMovieEarning = secondMovie[1].totalEarnings.slice(1,secondMovie[1].totalEarnings.length-1);

            if(firstMovieEarning > secondMovieEarning){
                return -1;
            }else if(firstMovieEarning < secondMovieEarning){
                return 1;
            }else{
                return 0;
            }
        }
    });
    return Object.fromEntries(sortedMoviesData);
};


const groupMoviesBasedOnGenre = (favouritesMovies) =>{

    let groupMovie =Object.entries(favouritesMovies).reduce((groupMovie,movie)=>{

        let genrePriority = {
            'drama' : 5,
            'sci-fi' : 4,
            'adventure': 3,
            'thriller' : 2,
            'crime' : 1
        };
        movie[1].genre.sort((genre1,genre2)=>{
            if(genrePriority[genre1] > genrePriority[genre2]){
                return -1;
            }else if(genrePriority[genre1] < genrePriority[genre2]){
                return 1;
            }else{
                return 0;
            }
        })
        if(groupMovie[movie[1].genre[0]] === undefined){
            groupMovie[movie[1].genre[0]] = [];
            groupMovie[movie[1].genre[0]].push(movie[0]);
        }else{
            groupMovie[movie[1].genre[0]].push(movie[0]);
        }
        return groupMovie;

    },{})
    return groupMovie;
};