const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

const usersHavingInteresetInVideoGames = (users) =>{
    let usersInterestInVideoGames = Object.entries(users).filter((user)=>{
        if(user[1].interests !== undefined){
            let interests = user[1].interests.toString().toLowerCase();
            return interests.includes("video games");
        }else{
            let interests = user[1].interest.toString().toLowerCase();
            return interests.includes("video games");
        }
    });
    return Object.fromEntries(usersInterestInVideoGames);
};

const usersStayingInGermany = (users) =>{
    let usersInGermany = Object.entries(users).filter((user)=>{
        return user[1].nationality === 'Germany';
    });
    return Object.fromEntries(usersInGermany);
};

const sortUsersBasedOnSeniorityLevel = (users) =>{

    const assignNumberToDesignation = (designation) =>{
      if(designation.includes("Senior")){
        return 3;
      }else if(designation.includes("Intern")){
        return 1;
      }else{
        return 2;
      }
    };

    let sortedUsers = Object.entries(users).sort((firstUser,secondUser)=>{

        let firstUserDesignation = assignNumberToDesignation(firstUser[1].desgination);
        let secondUserDesignation =  assignNumberToDesignation(secondUser[1].desgination);
        let firstUserAge = firstUser[1].age;
        let secondUserAge = secondUser[1].age;

        if(firstUserDesignation > secondUserDesignation){
            return -1;
        }else if(firstUserDesignation < secondUserDesignation){
            return 1
        }else{
            if(firstUserAge > secondUserAge){
                return -1;
            }else if(firstUserAge < secondUserAge){
                return 1
            }else{
                return 0;
            }
        }
    });
    return Object.fromEntries(sortedUsers);
};

const usersHavingMasterDegree = (users) =>{
    let usersMasterDegree = Object.entries(users).filter((user)=>{
        return user[1].qualification === 'Masters';
    });
    return Object.fromEntries(usersMasterDegree);
};

const groupUsersBasedOnProgrammingLanguage = (users) =>{

    let usersGroupByLanguage = Object.entries(users).reduce((groupByProgrammingLanguage,user)=>{

        let desgination = user[1].desgination;

        if(desgination.startsWith('Senior')){
            let splitDesignation= desgination.split(" ");
            let language = splitDesignation[1].trim();
            if(groupByProgrammingLanguage[language] === undefined){
                groupByProgrammingLanguage[language] = [];
                groupByProgrammingLanguage[language].push(user);
            }else{
                groupByProgrammingLanguage[language].push(user);   
            }
        }else if(desgination.startsWith('Intern')){
            let splitDesignation = desgination.split("-");
            let language = splitDesignation[1].trim();
            if(groupByProgrammingLanguage[language] === undefined){
                groupByProgrammingLanguage[language] = [];
                groupByProgrammingLanguage[language].push(user);
            }else{
                groupByProgrammingLanguage[language].push(user);   
            }
        }else{
            let splitDesignation = desgination.split(" ");
            let language = splitDesignation[0].trim();
            if(groupByProgrammingLanguage[language] === undefined){
                groupByProgrammingLanguage[language] = [];
                groupByProgrammingLanguage[language].push(user);
            }else{
                groupByProgrammingLanguage[language].push(user);   
            }
        }
        return groupByProgrammingLanguage;
    },{});
    return usersGroupByLanguage;
};
